import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Observable } from 'rxjs';

import { AnimeListRequest } from './models/anime_list_request'
import { UserDataRequest } from './models/user_data_request';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  public username: string = ""

  constructor(
    private http: HttpClient,
  ) { }

  public getUserData(): Observable<UserDataRequest> {
    return this.http.get<UserDataRequest>(`https://api.jikan.moe/v3/user/${this.username}`)
  }

  public getAnimeList(): Observable<AnimeListRequest> {
    return this.http.get<AnimeListRequest>(`https://api.jikan.moe/v3/user/${this.username}/animelist`)
  }
}

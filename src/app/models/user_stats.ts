export interface UserStats {
    "days_watched": number,
    "mean_score": number,
    "watching": number,
    "completed": number,
    "on_hold": number,
    "dropped": number,
    "plan_to_watch": number,
    "total_entries": number,
    "rewatched": number,
    "episodes_watched": number
}
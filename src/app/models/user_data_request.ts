import { UserStats } from "./user_stats";

export interface UserDataRequest {
    "request_hash": string,
    "request_cached": boolean,
    "request_cache_expiry": number,
    "user_id": number,
    "username": string,
    "url": string,
    "image_url": string,
    "last_online": string,
    "gender": string,
    "birthday": string,
    "location": string,
    "joined": string,
    "anime_stats": UserStats,
    "manga_stats": {
        "days_read": number,
        "mean_score": number,
        "reading": number,
        "completed": number,
        "on_hold": number,
        "dropped": number,
        "plan_to_read": number,
        "total_entries": number,
        "reread": number,
        "chapters_read": number,
        "volumes_read": number
    },
    "favorites": {
        "anime": [{
            "mal_id": number,
            "url": string,
            "image_url": string,
            "name": string
        }],
        "manga": [{
            "mal_id": number,
            "url": string,
            "image_url": string,
            "name": string
        }],
        "characters": [{
            "mal_id": number,
            "url": string,
            "image_url": string,
            "name": string
        }],
        "people": [{
            "mal_id": number,
            "url": string,
            "image_url": string,
            "name": string
        }]
    },
    "about": string
}
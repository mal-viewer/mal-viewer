import { Anime } from './anime'

export interface AnimeListRequest{
    "request_hash": string,
    "request_cached": boolean,
    "request_cache_expiry": number,
    "anime": Anime[]
}
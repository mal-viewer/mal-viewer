import { Component, OnInit } from '@angular/core';

import { ConnectionService } from 'src/app/connection.service';
import { Anime } from 'src/app/models/anime'
import { UserStats } from 'src/app/models/user_stats';
import { UserDataRequest } from 'src/app/models/user_data_request';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  title = 'mal-viewer';

  animelist!: Anime[];
  user_stats!: UserStats;
  user_username!: string;
  user_img!: string;

  loading: boolean = true;
  errorOccure: boolean = false;
  errorMessage = "Unexpected error";

  constructor(
    private ConnectionService: ConnectionService,
  ) {
  }

  ngOnInit() {

    this.ConnectionService.getUserData().subscribe(
      request => {
        this.user_stats = request.anime_stats
        this.user_username = request.username

        if (request.image_url === null) {
          this.user_img = 'assets/no_user.png'
        } else {
          this.user_img = request.image_url
        }

        this.ConnectionService.getAnimeList().subscribe(
          request => {
            this.animelist = request.anime
            this.loading = false
          }
        )
      }, error => {
        if (error.status == "404") {
          this.errorOccure = true
          this.errorMessage = "There's no such user"
        }
        else if (error.status == "503") {
          this.errorOccure = true
          this.errorMessage = "Service is temporarily unaviable. Try again in a few minutes"
        }

        this.loading = false
      }
    )
  }

  is_movie_rated(rate: string): boolean {
    if (parseInt(rate) !== 0) {
      return true
    } else {
      return false
    }
  }

  get_rate_color(rate: string) {
    if (parseInt(rate) >= 9) {
      return { background: "#00ff00" }
    } else if (parseInt(rate) >= 7) {
      return { background: "#80ff00" }
    } else if (parseInt(rate) >= 5) {
      return { background: "#f9f906" }
    } else if (parseInt(rate) >= 3) {
      return { background: "#ff8000" }
    } else {
      return { background: "#ff0000" }
    }
  }
}
